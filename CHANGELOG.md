0.0.2 / Mar 27, 2017
===================

* Fully functional input system
* Works on 64bit apps
* Varnam will be initialized using the language identifier
* Using libvarnam in a relative path
* Updated icons
* Hooked up learn and suggestions

0.0.1 / Mar 17, 2017
===================

* First release
