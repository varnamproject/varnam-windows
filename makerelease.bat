@ECHO OFF

SET version=0.0.2
SET target_dir=%TEMP%\varnam-ime

@RD /S /Q %target_dir%
mkdir %target_dir%
XCOPY ime\C++\SampleIME\deps\libvarnam\schemes %target_dir%\schemes /i
COPY "ime\C++\SampleIME\deps\libvarnam\Release\varnam.dll" %target_dir%\varnam.dll
COPY "ime\C++\x64\Release\VarnamIME.dll" %target_dir%\VarnamIME.dll
COPY install.bat %target_dir%\install.bat
"C:\Program Files\7-Zip\7z" a -t7z %TEMP%\varnam-ime-%version%.zip -r %target_dir%\*.*

pause